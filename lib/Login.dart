import "dart:convert";
import 'package:buckutt/Credentials.dart';
import "package:flutter/material.dart";
import "package:http/http.dart" as http;
import "Etu.dart";
import "Dashboard.dart";

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  var pin = new TextEditingController();
  var email = new TextEditingController();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  void _action() async {
      var cred = Credentials('baptiste.prunot@utt.fr', '1505');
      if (await cred.check()) {
          Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(builder: (context) => Dashboard(etudiant: cred.etu))
          );
      }else {
          Scaffold.of(context).showSnackBar(SnackBar(key: _scaffoldKey, content: Text("Wrong login")));
    }
  }

  @override
  void dispose() {
    email.dispose();
    pin.dispose();
    super.dispose();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text("Buckutt - Connexion"),
      ),
      body: Container(
          padding: EdgeInsets.all(12.0),
          child: Form(
            child: Column(children: _formComponent()),
          )),
    );
  }

  List<Widget> _formComponent() {
    return [
      TextFormField(
        decoration: InputDecoration(hintText: "email utt"),
        controller: email,
        keyboardType: TextInputType.emailAddress,
      ),
      TextField(
        decoration: InputDecoration(hintText: "code pin"),
        controller: pin,
        keyboardType: TextInputType.phone,
        obscureText: true,
      ),
      Center(
        child: RaisedButton(
          child: Text("Se connecter"),
          onPressed: _action,
        ),
      )
    ];
  }
}
