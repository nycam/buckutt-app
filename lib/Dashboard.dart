import "package:flutter/material.dart";
import "Etu.dart";
import "Login.dart";

class Dashboard extends StatelessWidget {
  final Etu etudiant;

  Dashboard({this.etudiant});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("BUCKUTT",
            style: TextStyle(color: Colors.white, letterSpacing: 15)),
        actions: [
          IconButton(
            icon: Icon(Icons.power_settings_new),
            color: Colors.white,
            onPressed: () {
              Navigator.pushReplacement(
                  context, MaterialPageRoute(builder: (context) => Login()));
            },
          )
        ],
      ),
      body: Center(child: _credit()),
    );
  }

  Widget _credit() {
    return Container(
        child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          "Credit restant",
          style: TextStyle(fontFamily: "Roboto", fontSize: 55),
        ),
        Text("${etudiant.credits}€", style: TextStyle(fontSize: 30)),
      ],
    ));
  }
}
