import 'dart:convert';
import 'package:buckutt/Credentials.dart';

class Etu {
  final String _lastname, _firstname, _mail;
  final int _credit;
  final String _token;

  get creditInCents => _credit;
  get credits => _credit / 100;

  Etu(this._lastname, this._firstname, this._mail, this._credit, this._token);

  factory Etu.fromJson(Map<String, dynamic> response) {
    var etu = response["user"];
    var token = response["token"];
    return Etu(etu["lastname"], etu["firstname"], etu["mail"], etu["credit"], token);
  }

  String toString() {
    return "${_firstname} ${_lastname} (${_mail}): ${_credit}";
  }
}
